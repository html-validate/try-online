import path from "node:path";
import { defineConfig } from "vite";
import browserslistToEsbuild from "browserslist-to-esbuild";

export default defineConfig({
	publicDir: false,
	resolve: {
		alias: {
			fs: path.resolve("./src/fs-stub.ts"),
		},
	},
	build: {
		outDir: "public",
		target: browserslistToEsbuild(),
	},
});
