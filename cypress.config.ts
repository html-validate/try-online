import { defineConfig } from "cypress";
import { type ConfigData } from "html-validate";
import { type CypressHtmlValidateOptions } from "cypress-html-validate";
import htmlvalidate from "cypress-html-validate/plugin";

export default defineConfig({
	video: false,
	e2e: {
		setupNodeEvents(on) {
			const config: ConfigData = {
				rules: {
					"require-sri": "off",
				},
			};
			const options: Partial<CypressHtmlValidateOptions> = {
				exclude: ["#editor", ".monaco-aria-container"],
			};
			htmlvalidate.install(on, config, options);
		},
		baseUrl: "http://localhost:8080/",
	},
});
