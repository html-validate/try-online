beforeEach(() => {
	cy.on("uncaught:exception", (error) => {
		if (error.message.includes("ResizeObserver")) {
			return false;
		} else {
			return true;
		}
	});
	cy.visit("/");
});

it("should load editor and present initial error", () => {
	cy.get("#loader", { timeout: 10000 }).should("not.exist");
	cy.get("#result li").should("have.length.greaterThan", 0);
	cy.get("#result li").should("contain", 'Redundant role "navigation" on <nav>');
});
