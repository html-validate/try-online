import { type IPosition, type MarkerSeverity } from "monaco-editor";
import { type Message } from "html-validate/browser";
import dedent from "dedent";
import { debounce } from "../debounce";
import { type ExtendedMessage } from "../validator";

interface Marker {
	startLineNumber: number;
	startColumn: number;
	endLineNumber: number;
	endColumn: number;
	severity: MarkerSeverity;
	message: string;
}

const markup = /* HTML */ `
	<!doctype html>
	<html lang="en">
		<head>
			<title></title>
			<link rel="stylesheet" href="style.css" />
		</head>
		<body>
			<header>
				<nav role="navigation" aria-labelledby="nav-label"></nav>
			</header>

			<main>
				<h1>Lorem & Ipsum Inc</h1>
				<p>dolor sit amet</p>

				<h2>Contact us</h2>
				<form>
					<label> Name: </label>
					<input type="name" />
				</form>

				<button><img src="submit.png" /></button>
			</main>

			<footer aria-hidden="true">
				<a href="#"></a>
			</footer>
		</body>
	</html>
`;

function mapSeverity(severity: number): MarkerSeverity {
	switch (severity) {
		case 2:
			return 8 satisfies MarkerSeverity.Error;
		case 1:
			return 4 satisfies MarkerSeverity.Warning;
		default:
			return 8 satisfies MarkerSeverity.Error;
	}
}

function createMarker(message: Message): Marker {
	return {
		startLineNumber: message.line,
		startColumn: message.column,
		endLineNumber: message.line,
		endColumn: message.column + message.size,
		severity: mapSeverity(message.severity),
		message: message.message,
	};
}

function positionInsideMessage(message: Message, position: IPosition): boolean {
	if (position.lineNumber !== message.line) {
		return false;
	}
	if (position.column < message.column) {
		return false;
	}
	if (position.column > message.column + message.size) {
		return false;
	}
	return true;
}

export async function init(
	target: HTMLElement,
	cb: (markup: string) => Promise<ExtendedMessage[]>,
): Promise<void> {
	let diagnostics: ExtendedMessage[] = [];

	const { editor: MonacoEditor, languages: MonacoLanguages } = await import("monaco-editor");

	MonacoLanguages.registerHoverProvider("html", {
		provideHover(_model, position) {
			const diagnostic = diagnostics.find((cur) => positionInsideMessage(cur, position));
			if (!diagnostic) {
				return { contents: [] };
			}

			const { markdown, message, ruleId, ruleUrl } = diagnostic;

			if (!markdown) {
				const contents = [`HTML-Validate`, message].join("\n\n");
				return { contents: [{ value: contents }] };
			}

			const contents = [
				`HTML-Validate`,
				markdown,
				ruleUrl ? `[${ruleId}](${ruleUrl})` : ruleId,
			].join("\n\n");

			return { contents: [{ value: contents }] };
		},
	});

	const editor = MonacoEditor.create(target, {
		value: dedent(markup),
		language: "html",
		codeLens: false,
		minimap: { enabled: false },
		roundedSelection: false,
		scrollBeyondLastLine: false,
		readOnly: false,
		theme: "vs-dark",
		automaticLayout: true,
	});

	async function validate(): Promise<void> {
		const model = editor.getModel();
		if (!model) {
			return;
		}

		diagnostics = await cb(editor.getValue());

		const markers = diagnostics.map(createMarker);
		MonacoEditor.setModelMarkers(model, "html-validate", markers);
	}

	const debouncedValidate = debounce(validate, 200);

	/* trigger validation after changing content */
	editor.onDidChangeModelContent(() => {
		debouncedValidate();
	});

	/* trigger initial validation */
	await validate();
}
