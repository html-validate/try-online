import EditorWorker from "monaco-editor/esm/vs/editor/editor.worker?worker";
import HtmlWorker from "monaco-editor/esm/vs/language/html/html.worker?worker";

self.MonacoEnvironment = {
	getWorker(_workerId, label) {
		switch (label) {
			case "html":
				return new HtmlWorker();
			default:
				return new EditorWorker();
		}
	},
};
