import "./editor/environment";

/* eslint-disable @typescript-eslint/no-non-null-assertion -- these should
 * always exist but if they don't we might just as well explode when we try to
 * use them */
const loader = document.querySelector("#loader")!;
const description = document.querySelector("#loader p")!;
const progress = document.querySelector("progress")!;
/* eslint-enable @typescript-eslint/no-non-null-assertion */

async function main(): Promise<void> {
	description.textContent = `Loading html-validate`;
	await import("html-validate/browser");
	progress.value = 33;
	progress.textContent = "33 %";

	description.textContent = `Loading monaco-editor`;
	await import("monaco-editor");
	progress.value = 66;
	progress.textContent = "66 %";

	description.textContent = `Loading application`;
	const { init } = await import("./index");
	await init();
	progress.value = 100;
	progress.textContent = "100 %";

	const animation = loader.animate([{ opacity: 1 }, { opacity: 0 }], {
		duration: 400,
		iterations: 1,
		fill: "forwards",
		easing: "ease-out",
	});

	animation.addEventListener("finish", () => {
		loader.remove();
	});
}

main().catch((err: unknown) => {
	/* eslint-disable-next-line no-console -- for debugging */
	console.error(err);
});
