import { type HtmlValidate, type Message } from "html-validate/browser";
import { type ExtendedMessage } from "./extended-message";

let htmlvalidate: HtmlValidate;

export async function init(): Promise<void> {
	const { HtmlValidate, StaticConfigLoader } = await import("html-validate/browser");

	const loader = new StaticConfigLoader({
		extends: ["html-validate:recommended", "html-validate:document", "html-validate:prettier"],
		elements: ["html5"],
	});
	htmlvalidate = new HtmlValidate(loader);
}

async function createExtendedMessage(message: Message): Promise<ExtendedMessage> {
	const documentation = await htmlvalidate.getContextualDocumentation(message);
	return { ...message, markdown: documentation?.description ?? null };
}

export async function validateString(markup: string): Promise<ExtendedMessage[]> {
	const report = await htmlvalidate.validateString(markup);
	const messages = report.results[0].messages;
	return Promise.all(messages.map(createExtendedMessage));
}
