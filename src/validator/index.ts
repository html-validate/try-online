export { type ExtendedMessage } from "./extended-message";
export { init, validateString } from "./validator";
