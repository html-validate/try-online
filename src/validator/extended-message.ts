import { type Message } from "html-validate/browser";

export interface ExtendedMessage extends Message {
	markdown: string | null;
}
