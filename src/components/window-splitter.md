# Window splitter

## Usage

```html
<div class="window-splitter">
  <div>primary content</div>
  <div role="separator"></div>
  <div>secondary content</div>
</div>
```

Use `aria-orientation` on `[role="separator"]` to control split orientation.
