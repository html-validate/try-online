import "./window-splitter.css";

declare global {
	interface HTMLElement {
		_windowSplitter: WindowSplitter;
	}
}

type Orientation = "horizontal" | "vertical";

function getOrientation(separator: HTMLElement): Orientation {
	const value = separator.ariaOrientation;
	switch (value) {
		case "vertical":
		case "horizontal":
			return value;
		default:
			return "horizontal";
	}
}

function calcMinSize(min: string, max: string, totalSize: number): number {
	return Math.max(parseInt(min, 10) || 0, totalSize - (parseInt(max, 10) || totalSize));
}

function calcMaxSize(max: string, min: string, totalSize: number): number {
	return Math.min(parseInt(max, 10) || totalSize, totalSize - (parseInt(min, 10) || 0));
}

function clamp(value: number, min: number, max: number): number {
	return Math.min(Math.max(value, min), max);
}

export class WindowSplitter {
	private separator: HTMLElement;
	private container: HTMLElement;
	private orientation: Orientation;

	public constructor(separator: HTMLElement, container: HTMLElement) {
		this.separator = separator;
		this.container = container;
		this.orientation = getOrientation(separator);

		this.initAttributes();
		this.initEvents();
		this.initMutationObserver();
	}

	private initAttributes(): void {
		const { separator } = this;
		const panes = this.getPanes();

		separator.setAttribute("tabindex", "1");
		separator.setAttribute("aria-orientation", this.orientation);

		const size = this.getMinMaxSize(panes);
		this.updateAriaValue(size);
	}

	private initEvents(): void {
		const { separator } = this;
		window.addEventListener("pointerdown", (event) => {
			this.onPointerDown(event);
		});

		separator.addEventListener("keydown", (event) => {
			this.onKeyDown(event);
		});
	}

	private initMutationObserver(): void {
		const { separator } = this;
		const observer = new MutationObserver((mutations) => {
			for (const mutation of mutations) {
				if (mutation.type === "attributes") {
					this.onAttributeChange();
				}
			}
		});

		observer.observe(separator, {
			attributes: true,
			attributeFilter: ["aria-orientation"],
		});
	}

	private onAttributeChange(): void {
		this.orientation = getOrientation(this.separator);
	}

	private onKeyDown(event: KeyboardEvent): void {
		const { orientation } = this;
		const { key } = event;

		const resize = this.createResizer();
		const amount = {
			ArrowLeft: -5,
			ArrowRight: 5,
			ArrowUp: -5,
			ArrowDown: 5,
		};

		switch (key) {
			case "ArrowLeft":
			case "ArrowRight": {
				event.preventDefault();
				if (orientation === "vertical") {
					resize(amount[key]);
				}
				break;
			}
			case "ArrowUp":
			case "ArrowDown": {
				event.preventDefault();
				if (orientation === "horizontal") {
					resize(amount[key]);
				}
				break;
			}
			case "Home":
				event.preventDefault();
				resize(Number.NEGATIVE_INFINITY);
				break;
			case "End":
				event.preventDefault();
				resize(Number.POSITIVE_INFINITY);
				break;
		}
	}

	private onPointerDown(event: PointerEvent): void {
		const { separator, orientation } = this;
		const { isPrimary, button, target, pointerId } = event;

		if (!isPrimary || button !== 0 || target !== separator) {
			return;
		}

		const property = orientation === "horizontal" ? "clientY" : "clientX";
		const reference = event[property];
		const resize = this.createResizer();

		function onPointerMove(event: PointerEvent): void {
			if (event.pointerId === pointerId) {
				resize(event[property] - reference);
			}
		}

		function onLostPointerCapture(event: PointerEvent): void {
			if (event.pointerId === pointerId) {
				separator.removeEventListener("pointermove", onPointerMove);
				separator.removeEventListener("lostpointercapture", onLostPointerCapture);
			}
		}

		onPointerMove(event);

		separator.addEventListener("lostpointercapture", onLostPointerCapture);
		separator.addEventListener("pointermove", onPointerMove);
		separator.setPointerCapture(pointerId);

		event.preventDefault();
	}

	private getPanes(): [HTMLElement, HTMLElement] {
		const { separator, container, orientation } = this;
		const pane1 = separator.previousElementSibling as HTMLElement;
		const pane2 = separator.nextElementSibling as HTMLElement;
		const style = getComputedStyle(container);
		const isReversed = style.flexDirection.endsWith("-reverse");
		const isRTL = orientation === "vertical" && style.direction === "rtl";
		if (isReversed && !isRTL) {
			return [pane2, pane1];
		} else {
			return [pane1, pane2];
		}
	}

	private getMinMaxSize(panes: [HTMLElement, HTMLElement]): {
		min: number;
		max: number;
		value: number;
		total: number;
	} {
		const { separator } = this;
		const [pane1, pane2] = panes;
		const pane1Style = getComputedStyle(pane1);
		const pane2Style = getComputedStyle(pane2);
		switch (this.orientation) {
			case "horizontal": {
				const value = pane1.offsetHeight;
				const total = pane1.offsetHeight + separator.offsetHeight + pane2.offsetHeight;
				const min = calcMinSize(pane1Style.minHeight, pane2Style.maxHeight, total);
				const max = calcMaxSize(pane1Style.maxHeight, pane2Style.minHeight, total);
				return { min, max, value, total };
			}
			case "vertical": {
				const value = pane1.offsetWidth;
				const total = pane1.offsetWidth + separator.offsetWidth + pane2.offsetWidth;
				const min = calcMinSize(pane1Style.minWidth, pane2Style.maxWidth, total);
				const max = calcMaxSize(pane1Style.maxWidth, pane2Style.minWidth, total);
				return { min, max, value, total };
			}
		}
	}

	private updateAriaValue({ min, max, value }: { min: number; max: number; value: number }): void {
		const { separator } = this;
		separator.setAttribute("aria-valuemin", String(min));
		separator.setAttribute("aria-valuemax", String(max));
		separator.setAttribute("aria-valuenow", String(value));
	}

	private createResizer(): (movement: number) => { min: number; max: number; value: number } {
		const { orientation } = this;
		const panes = this.getPanes();
		const [pane1, pane2] = panes;
		const { min, max, value, total } = this.getMinMaxSize(panes);
		switch (orientation) {
			case "horizontal": {
				return (movement: number) => {
					const updatedValue = clamp(value + movement, min, max);
					pane1.style.flexBasis = `${String(updatedValue)}px`;
					pane2.style.flexBasis = `${String(total - updatedValue)}px`;
					this.updateAriaValue({ min, max, value: updatedValue });
					return { min, max, value: updatedValue };
				};
			}
			case "vertical": {
				return (movement: number) => {
					const updatedValue = clamp(value + movement, min, max);
					pane1.style.flexBasis = `${String(updatedValue)}px`;
					pane2.style.flexBasis = `${String(total - updatedValue)}px`;
					this.updateAriaValue({ min, max, value: updatedValue });
					return { min, max, value };
				};
			}
		}
	}
}

export function windowSplitter(separator: HTMLElement, container: HTMLElement): WindowSplitter {
	const splitter = new WindowSplitter(separator, container);
	separator._windowSplitter = splitter;
	return splitter;
}
