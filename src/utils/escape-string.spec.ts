it("should return text", () => {
	expect.assertions(1);
	expect(escape("foo")).toBe("foo");
});

it("should encode &", () => {
	expect.assertions(1);
	expect(escape("foo & bar")).toMatchInlineSnapshot(`"foo%20%26%20bar"`);
});

it("should encode <foo>", () => {
	expect.assertions(1);
	expect(escape("<foo>")).toMatchInlineSnapshot(`"%3Cfoo%3E"`);
});
