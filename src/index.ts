import { windowSplitter } from "./components";
import { init as initEditor } from "./editor";
import { problems } from "./problems";
import { type ExtendedMessage, init as initValidator, validateString } from "./validator";
import "./style.scss";

/* eslint-disable-next-line @typescript-eslint/no-unnecessary-type-parameters -- while maybe bad design it is made to mimic document.querySelector */
function querySelector<T extends Element>(selector: string): T {
	const element = document.querySelector<T>(selector);
	if (!element) {
		throw new Error(`Failed to locate "${selector}`);
	}
	return element;
}

export async function init(): Promise<void> {
	const selector = ".window-splitter [role=separator]";
	for (const separator of document.querySelectorAll<HTMLElement>(selector)) {
		const container = separator.parentElement;
		if (!container) {
			continue;
		}
		windowSplitter(separator, container);
	}

	await initValidator();
	const validate = createValidator();

	const editor = querySelector<HTMLElement>("#editor");
	await initEditor(editor, (markup: string) => {
		return validate(markup);
	});
}

function createValidator(): (markup: string) => Promise<ExtendedMessage[]> {
	const target = querySelector<HTMLElement>("#result");

	async function inner(markup: string): Promise<ExtendedMessage[]> {
		try {
			target.innerHTML = "";

			const messages = await validateString(markup);
			const problemElement = problems(messages);
			target.appendChild(problemElement);
			return messages;
		} catch (error) {
			if (error instanceof Error) {
				target.innerText = error.message;
			} else {
				/* eslint-disable-next-line no-console -- for debugging */
				console.error(error);
				target.innerText = "An unknown error occured, check log";
			}
			return [];
		}
	}

	return inner;
}
