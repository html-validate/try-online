import { type Message } from "html-validate/browser";
import { escapeString } from "./utils";

function mapSeverity(message: Message): "error" | "warning" {
	return message.severity === 1 ? "warning" : "error";
}

function markup(message: Message): string {
	const text = escapeString(message.message);
	const line = String(message.line);
	const column = String(message.column);
	const severity = mapSeverity(message);
	return /* HTML */ `
		<div class="problem problem--${severity}">
			<span class="problem__icon codicon codicon-error"></span>
			<span class="problem__message"> ${text} </span>
			<span class="problem__source"> ${message.ruleId} </span>
			<span class="problem__location"> [Ln ${line}, Col: ${column}] </span>
		</div>
	`;
}

export function problems(messages: Message[]): HTMLElement {
	const ul = document.createElement("ul");
	ul.classList.add("problem-list");
	for (const entry of messages) {
		const li = document.createElement("li");
		li.classList.add("problem-list__item");
		li.innerHTML = markup(entry);
		ul.appendChild(li);
	}
	return ul;
}
