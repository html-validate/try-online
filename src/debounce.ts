export function debounce<TThis, TArgs extends unknown[]>(
	fn: (this: TThis, ...args: TArgs) => unknown,
	timeout: number,
): (this: TThis, ...args: TArgs) => void {
	let timer: ReturnType<typeof setTimeout> | undefined;

	return function (this: TThis, ...args: TArgs) {
		if (timer !== undefined) {
			clearTimeout(timer);
		}

		timer = setTimeout(() => {
			timer = undefined;
			fn.apply(this, args);
		}, timeout);
	};
}
